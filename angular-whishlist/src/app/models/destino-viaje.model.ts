import {v4 as uuid} from 'uuid';
export class DestinoViaje{

	private selected: boolean;

	public servicios: string[];

	public id = uuid();

	public votes = 0;

	constructor(public nombre:string, public imagenUrl:string){
		this.servicios=['desayuno','almuerzo','cena','limpieza']
	}
	isSelected():boolean{
		return this.selected;
	}
	setSelected(s:boolean){
		this.selected=s;
	}
	voteUp():any{
		this.votes++;
	}
	voteDown():any{
		this.votes--;
	}

	voteReset(): any{
		this.votes=0;
	}
}